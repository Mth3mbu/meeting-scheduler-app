﻿using System;
using System.Collections.Generic;
using Meeting.Manager.API.Interfaces;
using Meeting.Manager.API.Models;
using Meeting.Manager.API.Repositories;
using Meeting.Manager.API.Services;
using NSubstitute;
using NUnit.Framework;

namespace Meeting.Manager.Tests
{
    [TestFixture]
    public class MeetingItemServiceTests
    {
        private readonly IMeetingItemRepository _meetingItemRepository = Substitute.For<IMeetingItemRepository>();
        private readonly IMeetingItemService _meetingItemService;
        private const int CreateResponse = 1;

        private readonly List<Item> _meetingItems = new List<Item>
        {
           new Item{Description = "Testing 01",DueDate = DateTime.MinValue,Id = Guid.Empty,IsCarried =false,PersonAssigned = "Test o1",Status = "To do",StatusId = Guid.Empty},
           new Item{Description = "Testing 02",DueDate = DateTime.MinValue,Id = Guid.Empty,IsCarried =false,PersonAssigned = "Test o1",Status = "To do",StatusId = Guid.Empty},
           new Item{Description = "Testing 03",DueDate = DateTime.MinValue,Id = Guid.Empty,IsCarried =false,PersonAssigned = "Test o1",Status = "To do",StatusId = Guid.Empty}
        };

        private readonly List<Status> _statuses = new List<Status>
        {
            new Status
            {
                Id = Guid.Empty,
                Name = "To do"
            },
            new Status
            {
                Id = Guid.Empty,
                Name = "In progress"
            },
            new Status
            {
                Id = Guid.Empty,
                Name = "Complete"
            }
        };

        public MeetingItemServiceTests()
        {
            _meetingItemService = new MeetingItemService(_meetingItemRepository);
        }

        [Test]
        public void For_Service_CreateMeetingItem_Given_MeetingItems_And_MeetingId_Should_Return_One()
        {
            _meetingItemRepository.CreatMeetingItem(Arg.Any<Item>()).Returns(CreateResponse);

            var results = _meetingItemService.CreatMeetingItem(new Item(), Guid.Empty);

            Assert.AreEqual(1, CreateResponse);
        }

        [Test]
        public void For_Service_GetPreviousMeetingItems_Given_MeetingId_Should_Return_Previous_MeetingItems_Of_Same_Type()
        {
            _meetingItemRepository.GetPreviousMeetingItemsByMeetingTypeId(Arg.Any<Guid>()).Returns(_meetingItems);

            var results = _meetingItemService.GetPreviousMeetingItems(Guid.Empty);

            Assert.AreEqual(3, results.Count);
            Assert.AreSame(_meetingItems, results);
        }

        [Test]
        public void For_Service_GetMeetingItemsByMeetingId_Given_MeetingId_Should_Return_MeetingItems()
        {
            _meetingItemRepository.GetMeetingItemsByMeetingId(Arg.Any<Guid>()).Returns(_meetingItems);

            var results = _meetingItemService.GetMeetingItemsByMeetingId(Guid.Empty);

            Assert.AreEqual(3, results.Count);
            Assert.AreSame(_meetingItems, results);
        }

        [Test]
        public void For_Service_GetAllMeetingStatuses_Given_Nothing_Should_Return_AllStatuses()
        {
            _meetingItemRepository.GetAllMeetingStatuses().Returns(_statuses);

            var results = _meetingItemService.GetAllMeetingStatuses();

            Assert.AreEqual(3, results.Count);
            Assert.AreSame(_statuses, results);
        }

        [Test]
        public void For_Service_UpdateMeetingItemStatus_Given_Status_Should_Return_One()
        {
            _meetingItemRepository.GetAllMeetingStatuses().Returns(_statuses);

            var results = _meetingItemService.GetAllMeetingStatuses();

            Assert.AreEqual(3, results.Count);
            Assert.AreSame(_statuses, results);
        }
    }
}
