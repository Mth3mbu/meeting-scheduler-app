﻿using System;
using System.Collections.Generic;
using Meeting.Manager.API.Interfaces;
using Meeting.Manager.API.Services;
using NSubstitute;
using NUnit.Framework;

namespace Meeting.Manager.Tests
{
    [TestFixture]
    public class MeetingServiceTests
    {
        private readonly IMeetingRepository _meetingRepository = Substitute.For<IMeetingRepository>();
        private readonly IMeetingService _meetingService;
        private const int CreateResponse = 1;
        private const int Expected = CreateResponse;
        private readonly List<API.Models.Meeting> _meetings = new List<API.Models.Meeting>
        {
            new API.Models.Meeting{DateCreated = new DateTime(),Id = Guid.NewGuid(),Type = "Finance",TypeId = Guid.NewGuid()},
            new API.Models.Meeting{DateCreated = new DateTime(),Id = Guid.NewGuid(),Type = "Manco",TypeId = Guid.NewGuid()},
            new API.Models.Meeting{DateCreated = new DateTime(),Id = Guid.NewGuid(),Type = "Project Team Leaders",TypeId = Guid.NewGuid()},
        };

        private readonly List<API.Models.Type> _meetingTypes = new List<API.Models.Type>
        {
            new API.Models.Type
            {
                Id = new Guid(),
                Name = "Finance"
            },
            new API.Models.Type
            {
                Id = new Guid(),
                Name = "Manco"
            },
            new API.Models.Type
            {
                Id = new Guid(),
                Name = "Project Team Leaders "
            },
        };

        public MeetingServiceTests()
        {
            _meetingService = new MeetingService(_meetingRepository);
        }

        [Test]
        public void For_Service_CreateMeeting_Given_Meetings_Should_ReturnAllOne()
        {
            _meetingRepository.CreateMeeting(Arg.Any<API.Models.Meeting>()).Returns(CreateResponse);

            var results = _meetingService.CreateMeeting(new API.Models.Meeting());

            Assert.AreEqual(Expected, results);
        }

        [Test]
        public void For_Service_GetAllMeeting_Given_Nothing_Should_ReturnAllMeetings()
        {
            _meetingRepository.GetAllMeetings().Returns(_meetings);

            var results = _meetingService.GetAllMeetings();

            Assert.AreEqual(3, results.Count);
            Assert.AreSame(_meetings, results);
        }


        [Test]
        public void For_Service_GetAllMeetingTypes_Given_Nothing_Should_ReturnAllMeetingTypes()
        {
            _meetingRepository.GetAllMeetingTypes().Returns(_meetingTypes);

            var results = _meetingService.GetAllMeetingTypes();

            Assert.AreEqual(3, results.Count);
            Assert.AreSame(_meetingTypes, results);
        }
    }
}
