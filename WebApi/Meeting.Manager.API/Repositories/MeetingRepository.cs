﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Meeting.Manager.API.Interfaces;
using Type = Meeting.Manager.API.Models.Type;

namespace Meeting.Manager.API.Repositories
{
    public class MeetingRepository : IMeetingRepository
    {
        private readonly SqlConnection _connection;
        public MeetingRepository(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        public int CreateMeeting(Models.Meeting meeting)
        {
            var param = new
            {
                id = meeting.Id,
                typeId = meeting.TypeId,
                name = meeting.Name,
                date = meeting.DateCreated
            };

            return _connection.Execute("spInsertMeeting", param, commandType: CommandType.StoredProcedure);
        }

        public List<Models.Meeting> GetAllMeetings()
        {
            return _connection.Query<Models.Meeting>("spGetAllMeetings", commandType: CommandType.StoredProcedure).ToList();
        }

        public List<Type> GetAllMeetingTypes()
        {
            return _connection.Query<Type>("spGetAllMeetingTypes", commandType: CommandType.StoredProcedure).ToList();
        }

        public Models.Meeting GetMeetingByItemId(Guid itemId)
        {
            var param = new { itemId };
            return _connection.QueryFirst<Models.Meeting>("spGEtMeetingByItemId",param, commandType: CommandType.StoredProcedure);
        }
    }
}