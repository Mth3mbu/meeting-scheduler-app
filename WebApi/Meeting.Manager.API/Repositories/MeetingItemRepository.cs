﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Meeting.Manager.API.Interfaces;
using Meeting.Manager.API.Models;

namespace Meeting.Manager.API.Repositories
{
    public class MeetingItemRepository : IMeetingItemRepository
    {
        private readonly SqlConnection _connection;
        public MeetingItemRepository(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        public int CreatMeetingItem(Item item)
        {
            var param = new
            {
                id = item.Id,
                statusId = item.StatusId,
                personAssigned = item.PersonAssigned,
                description = item.Description,
                dueDate = item.DueDate
            };

            return _connection.Execute("spInsertMeetingItem", param, commandType: CommandType.StoredProcedure);
        }

        public List<Item> GetPreviousMeetingItemsByMeetingTypeId(Guid meetingTypeId)
        {
            var param = new { typeId = meetingTypeId };
            return _connection.Query<Item>("spGetPreviousMeetingItemsByMeetingType", param, commandType: CommandType.StoredProcedure).ToList();
        }

        public List<Item> GetMeetingItemsByMeetingId(Guid meetingId)
        {
            var param = new { meetingId };
            return _connection.Query<Item>("spGetAllMeetingsItemsByMeetingId", param, commandType: CommandType.StoredProcedure).ToList();
        }

        public int UpdateMeetingItemStatus(Item item)
        {
            var param = new { id = item.Id, statusId = item.StatusId };
            return _connection.Execute("spUpdateMeetingItemStatus", param, commandType: CommandType.StoredProcedure);
        }

        public int LinkMeetingItem(Guid meetingId, Guid[] itemIds, bool isCarried)
        {
            var param = new
            {
                items = GetItems(itemIds, meetingId, isCarried).AsTableValuedParameter("ItemsUDT")
            };
            return _connection.Execute("spLinkMeetingItems", param, commandType: CommandType.StoredProcedure);
        }

        public List<Status> GetAllMeetingStatuses()
        {
            return _connection.Query<Status>("spGetStatuses", commandType: CommandType.StoredProcedure).ToList();
        }

        public int CreateItemStatusHistory(Guid itemId, Guid statusId)
        {
            var param = new { itemId, statusId };
            return _connection.Execute("spInsertStatusHistory", param, commandType: CommandType.StoredProcedure);
        }

        private DataTable GetItems(Guid[] items, Guid meetingId, bool isCarried)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("Id", typeof(Guid));
            dataTable.Columns.Add("MeetingId", typeof(Guid));
            dataTable.Columns.Add("ItemId", typeof(Guid));
            dataTable.Columns.Add("Iscarried", typeof(bool));
            dataTable.Columns.Add("DateCreated", typeof(DateTime));

            foreach (var itemId in items)
            {
                dataTable.Rows.Add(Guid.NewGuid(), meetingId, itemId, isCarried, DateTime.Now);
            }

            return dataTable;
        }
    }
}