﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Meeting.Manager.API.Interfaces;
using Meeting.Manager.API.Repositories;
using Meeting.Manager.API.Services;
using Meeting.Manager.API.Utils;

namespace Meeting.Manager.API
{
    public class AutofacWebapiConfig
    {
        private static IContainer _container;
        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        private static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            RegisterRepositoryTypes(builder);
            RegisterServiceTypes(builder);
            _container = builder.Build();

            return _container;
        }

        private static void RegisterRepositoryTypes(ContainerBuilder builder)
        {
            builder.RegisterType<MeetingItemRepository>()
                .As<IMeetingItemRepository>()
                .InstancePerLifetimeScope()
                .WithParameter("connectionString", ConfigUtils.MeetingManagerConnectionString);

            builder.RegisterType<MeetingRepository>()
                .As<IMeetingRepository>()
                .InstancePerLifetimeScope()
                .WithParameter("connectionString", ConfigUtils.MeetingManagerConnectionString);
        }

        private static void RegisterServiceTypes(ContainerBuilder builder)
        {
            builder.RegisterType<MeetingItemService>()
                .As<IMeetingItemService > ()
                .InstancePerLifetimeScope();

            builder.RegisterType<MeetingService>()
                .As<IMeetingService>()
                .InstancePerLifetimeScope();
        }
    }
}