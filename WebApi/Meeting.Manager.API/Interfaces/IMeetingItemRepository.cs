﻿using System;
using System.Collections.Generic;
using Meeting.Manager.API.Models;

namespace Meeting.Manager.API.Interfaces
{
    public interface IMeetingItemRepository
    {
        int CreatMeetingItem(Item item);
        List<Item> GetPreviousMeetingItemsByMeetingTypeId(Guid meetingTypeId);
        List<Item> GetMeetingItemsByMeetingId(Guid meetingId);
        int UpdateMeetingItemStatus(Item item);
        int LinkMeetingItem(Guid meetingId, Guid[] itemId, bool isCarried);
        List<Status> GetAllMeetingStatuses();
        int CreateItemStatusHistory(Guid itemId, Guid statusId);
    }
}
