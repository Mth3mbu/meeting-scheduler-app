﻿using System;
using System.Collections.Generic;
using Meeting.Manager.API.Models;

namespace Meeting.Manager.API.Interfaces
{
    public interface IMeetingItemService
    {
        int CreatMeetingItem(Item item, Guid meetingId);
        List<Item> GetPreviousMeetingItems(Guid meetingId);
        List<Item> GetMeetingItemsByMeetingId(Guid meetingId);
        int UpdateMeetingItemStatus(Item item);
        List<Status> GetAllMeetingStatuses();
        int LinkMeetingItems(Guid[] itemIds, Guid meetingId, bool isCarried);
    }
}
