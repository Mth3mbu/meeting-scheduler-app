﻿using System;
using System.Collections.Generic;
using Type = Meeting.Manager.API.Models.Type;

namespace Meeting.Manager.API.Interfaces
{
    public interface IMeetingService
    {
        int CreateMeeting(Models.Meeting meeting);
        List<Models.Meeting> GetAllMeetings();
        List<Type> GetAllMeetingTypes();
        Models.Meeting GetMeetingByItemId(Guid itemId);
    }
}
