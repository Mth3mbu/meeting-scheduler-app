﻿using System;
using System.Collections.Generic;
using Meeting.Manager.API.Interfaces;
using Type = Meeting.Manager.API.Models.Type;

namespace Meeting.Manager.API.Services
{
    public class MeetingService : IMeetingService
    {
        private readonly IMeetingRepository _meetingRepository;
        public MeetingService(IMeetingRepository meetingRepository)
        {
            _meetingRepository = meetingRepository;
        }

        public int CreateMeeting(Models.Meeting meeting)
        {
            return _meetingRepository.CreateMeeting(meeting);
        }

        public List<Models.Meeting> GetAllMeetings()
        {
            return _meetingRepository.GetAllMeetings();
        }

        public List<Type> GetAllMeetingTypes()
        {
            return _meetingRepository.GetAllMeetingTypes();
        }

        public Models.Meeting GetMeetingByItemId(Guid itemId)
        {
            return _meetingRepository.GetMeetingByItemId(itemId);
        }
    }
}