﻿using System;
using System.Collections.Generic;
using Meeting.Manager.API.Interfaces;
using Meeting.Manager.API.Models;

namespace Meeting.Manager.API.Services
{
    public class MeetingItemService : IMeetingItemService
    {
        private readonly IMeetingItemRepository _meetingItemRepository;
        public MeetingItemService(IMeetingItemRepository meetingItemRepository)
        {
            _meetingItemRepository = meetingItemRepository;
        }

        public int CreatMeetingItem(Item item, Guid meetingId)
        {
            item.Id = Guid.NewGuid();
            var results = _meetingItemRepository.CreatMeetingItem(item);
            if (results <= 0) return results;

            _meetingItemRepository.LinkMeetingItem(meetingId, new[] { item.Id }, item.IsCarried);
            _meetingItemRepository.CreateItemStatusHistory(item.Id, item.StatusId);

            return results;
        }

        public List<Item> GetPreviousMeetingItems(Guid meetingId)
        {
            return _meetingItemRepository.GetPreviousMeetingItemsByMeetingTypeId(meetingId);
        }

        public List<Item> GetMeetingItemsByMeetingId(Guid meetingId)
        {
            return _meetingItemRepository.GetMeetingItemsByMeetingId(meetingId);
        }

        public int UpdateMeetingItemStatus(Item item)
        {
            var results = _meetingItemRepository.UpdateMeetingItemStatus(item);
            if (results > 0)
                _meetingItemRepository.CreateItemStatusHistory(item.Id, item.StatusId);

            return results;
        }

        public List<Status> GetAllMeetingStatuses()
        {
            return _meetingItemRepository.GetAllMeetingStatuses();
        }

        public int LinkMeetingItems(Guid[] itemIds, Guid meetingId, bool isCarried)
        {
            return _meetingItemRepository.LinkMeetingItem(meetingId, itemIds, isCarried);
        }
    }
}