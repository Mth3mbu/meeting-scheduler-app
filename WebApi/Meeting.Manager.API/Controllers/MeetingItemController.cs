﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Meeting.Manager.API.Interfaces;
using Meeting.Manager.API.Models;

namespace Meeting.Manager.API.Controllers
{
    [RoutePrefix("api/meeting/item/module")]
    public class MeetingItemController : ApiController
    {
        private readonly IMeetingItemService _meetingItemService;
        public MeetingItemController(IMeetingItemService meetingItemService)
        {
            _meetingItemService = meetingItemService;
        }

        [HttpPost]
        [Route("{meetingId}")]
        public HttpResponseMessage CreateMeeting([FromBody] Item item, Guid meetingId)
        {
            return Request.CreateResponse(HttpStatusCode.Created, _meetingItemService.CreatMeetingItem(item, meetingId));
        }

        [HttpPost]
        [Route("link/{meetingId}")]
        public HttpResponseMessage LinkMeetingItems([FromBody] Guid[] itemIds, Guid meetingId, bool isCarried)
        {
            return Request.CreateResponse(HttpStatusCode.Created, _meetingItemService.LinkMeetingItems(itemIds, meetingId, isCarried));
        }

        [HttpGet]
        [Route("{typeId}")]
        public HttpResponseMessage GetPreviousMeetingItems(Guid typeId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _meetingItemService.GetPreviousMeetingItems(typeId));
        }

        [HttpGet]
        [Route("{meetingId}/items")]
        public HttpResponseMessage GeMeetingItemsByMeetingId(Guid meetingId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _meetingItemService.GetMeetingItemsByMeetingId(meetingId));
        }

        [HttpPut]
        [Route("")]
        public HttpResponseMessage UpdateItem(Item item)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _meetingItemService.UpdateMeetingItemStatus(item));
        }

        [HttpGet]
        [Route("statuses")]
        public HttpResponseMessage GetAllStatuses()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _meetingItemService.GetAllMeetingStatuses());
        }
    }
}