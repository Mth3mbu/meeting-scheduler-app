﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Meeting.Manager.API.Interfaces;

namespace Meeting.Manager.API.Controllers
{
    [RoutePrefix("api/meeting")]
    public class MeetingController : ApiController
    {
        private readonly IMeetingService _meetingService;
        public MeetingController(IMeetingService meetingService)
        {
            _meetingService = meetingService;
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage CreateMeeting([FromBody] Models.Meeting meeting)
        {
            return Request.CreateResponse(HttpStatusCode.Created, _meetingService.CreateMeeting(meeting));
        }

        [HttpGet]
        [Route("")]
        public HttpResponseMessage GetAllMeetings()
        {
            return Request.CreateResponse(HttpStatusCode.Created, _meetingService.GetAllMeetings());
        }

        [HttpGet]
        [Route("types")]
        public HttpResponseMessage GetAllMeetingTypes()
        {
            return Request.CreateResponse(HttpStatusCode.Created, _meetingService.GetAllMeetingTypes());
        }

        [HttpGet]
        [Route("{itemId}")]
        public HttpResponseMessage GetAllMeetingByMeetingId(Guid itemId)
        {
            return Request.CreateResponse(HttpStatusCode.Created, _meetingService.GetMeetingByItemId(itemId));
        }
    }
}