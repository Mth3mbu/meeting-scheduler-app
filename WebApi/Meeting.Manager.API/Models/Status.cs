﻿using System;

namespace Meeting.Manager.API.Models
{
    public class Status
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}