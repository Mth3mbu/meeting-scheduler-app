﻿using System;

namespace Meeting.Manager.API.Models
{
    public class Meeting
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid TypeId { get; set; }
        public string Type { get; set; }
        public DateTime DateCreated { get; set; }
    }
}