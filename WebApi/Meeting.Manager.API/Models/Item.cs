﻿using System;

namespace Meeting.Manager.API.Models
{
    public class Item
    {
        public Guid Id { get; set; }
        public Guid StatusId { get; set; }
        public DateTime DueDate { get; set; }
        public string PersonAssigned { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public bool IsCarried { get; set; }
    }
}