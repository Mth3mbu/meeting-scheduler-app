﻿using System.Configuration;

namespace Meeting.Manager.API.Utils
{
    public static class ConfigUtils
    {
        public static string MeetingManagerConnectionString =>
            ConfigurationManager.ConnectionStrings["MeetingManager"].ConnectionString;
    }
}