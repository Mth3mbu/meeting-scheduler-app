import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeetingComponent } from '../components/meeting/meeting.component';
import { MeetingListingComponent } from '../components/meeting-listing/meeting-listing.component';
import { MeetingDetailsComponent } from '../components/meeting-details/meeting-details.component';

const routes: Routes = [
  { path: 'details', component:  MeetingDetailsComponent},
    { path: 'meeting', component:  MeetingComponent},
    { path: '', component:  MeetingListingComponent}
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }