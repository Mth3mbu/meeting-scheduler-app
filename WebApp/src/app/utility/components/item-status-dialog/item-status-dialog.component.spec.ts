import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemStatusDialogComponent } from './item-status-dialog.component';

describe('ItemStatusDialogComponent', () => {
  let component: ItemStatusDialogComponent;
  let fixture: ComponentFixture<ItemStatusDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemStatusDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemStatusDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
