import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IStatusDialogData } from '../../interfaces/items-dialog-data';

@Component({
  selector: 'app-item-status-dialog',
  templateUrl: './item-status-dialog.component.html',
  styleUrls: ['./item-status-dialog.component.css']
})
export class ItemStatusDialogComponent{
  constructor(
    public dialogRef: MatDialogRef<ItemStatusDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:IStatusDialogData) {
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
