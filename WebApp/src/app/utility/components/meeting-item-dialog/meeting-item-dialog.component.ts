import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IMeetingItem } from '../../interfaces/item';

@Component({
  selector: 'app-meeting-item-dialog',
  templateUrl: './meeting-item-dialog.component.html',
  styleUrls: ['./meeting-item-dialog.component.css']
})
export class MeetingItemDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<MeetingItemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IMeetingItem) { }

  ngOnInit(): void {
  }
 
  onCancelClick(): void {
    this.dialogRef.close();
  }
}
