import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingItemDialogComponent } from './meeting-item-dialog.component';

describe('MeetingItemDialogComponent', () => {
  let component: MeetingItemDialogComponent;
  let fixture: ComponentFixture<MeetingItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingItemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
