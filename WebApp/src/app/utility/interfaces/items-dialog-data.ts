import { IStatus } from './status';
export interface IStatusDialogData {
    id:string;
    statuses: IStatus[];
  }