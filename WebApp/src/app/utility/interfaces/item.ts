import { IStatus } from './status';
export interface IMeetingItem{
    Id:string,
    DueDate :Date,
    PersonAssigned:string,
    Description :string,
    Status:string,
    IsCarried:boolean,
    StatusId:string
    Statuses:IStatus[]
}