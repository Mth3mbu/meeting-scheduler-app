export interface IMeeting{
    Id:string,
    TypeId:string,
    Type:string,
    Name:string
    DateCreated:string
}