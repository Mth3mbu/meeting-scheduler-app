import { Injectable } from '@angular/core';
import { IStatus } from './../interfaces/status';
import { timeout, catchError } from 'rxjs/operators';
import { IMeetingItem } from './../interfaces/item';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ITemService {
  apiUrl: string;
  constructor(private httpClient: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  forwardMeetingItems(itemIds: string[], meetingId: string,isCarried) {
    return this.httpClient
      .post(`${this.apiUrl}/item/module/link/${meetingId}?isCarried=${isCarried}`, itemIds)
      .pipe(
        timeout(10000),
        catchError((error: HttpErrorResponse) => {
          return throwError(error);
        })
      );
  }

  createMeetingItem(item: IMeetingItem,meetingId:string) {
    return this.httpClient.post(`${this.apiUrl}/item/module/${meetingId}`, item).pipe(
      timeout(10000),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  updateMeetingItem(item:IMeetingItem){
    return this.httpClient.put(`${this.apiUrl}/item/module`,item).pipe(
      timeout(1000),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }
  
  getPreviousMeengItems(typeId: string) {
    return this.httpClient
      .get<IMeetingItem[]>(`${this.apiUrl}/item/module/${typeId}`)
      .pipe(
        timeout(10000),
        catchError((error: HttpErrorResponse) => {
          return throwError(error);
        })
      );
  }
 
  getMeetingItemsByMeetingId(meetingId:string){
    return this.httpClient
    .get<IMeetingItem[]>(`${this.apiUrl}/item/module/${meetingId}/items`)
    .pipe(
      timeout(10000),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  getStatuses() {
    return this.httpClient.get<IStatus[]>(`${this.apiUrl}/item/module/statuses`).pipe(
      timeout(10000),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }
}
