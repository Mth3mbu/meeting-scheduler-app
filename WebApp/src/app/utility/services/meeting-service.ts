import { IType } from './../interfaces/type';
import { IMeeting } from './../interfaces/meeting';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { timeout, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class MeetingService {
  apiUrl: string;

  constructor(private httpClient: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  getAllMeetings() {
    return this.httpClient.get<IMeeting[]>(`${this.apiUrl}`).pipe(
      timeout(20000),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  getMeetingTypes(){
    return this.httpClient.get<IType[]>(`${this.apiUrl}/types`).pipe(
        timeout(20000),
        catchError((error: HttpErrorResponse) => {
          return throwError(error);
        })
      );
  }

  CreateMeeting(meeting:IMeeting){
      return this.httpClient.post(`${this.apiUrl}`,meeting).pipe(
        timeout(10000),
        catchError((error: HttpErrorResponse) => {
          return throwError(error);
        })
      );
  }

  getMeetingByItemId(itemId) {
    return this.httpClient.get<IMeeting>(`${this.apiUrl}/${itemId}`).pipe(
      timeout(10000),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }
}
