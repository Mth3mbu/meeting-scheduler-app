import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ItemStatusDialogComponent } from '../components/item-status-dialog/item-status-dialog.component';
import { IStatusDialogData } from '../interfaces/items-dialog-data';
import { MeetingItemDialogComponent } from '../components/meeting-item-dialog/meeting-item-dialog.component';

@Injectable({
    providedIn: 'root'
})
export class DialogService {
    constructor(private dialog: MatDialog) { }

    openDialog(data) {
        const dialogRef = this.dialog.open(MeetingItemDialogComponent, {
            width: '50em',
            data: {
                DueDate:data.DueDate,
                PersonAssigned: data.PersonAssigned,Description:data.Description ,
                StatusId: data.StatusId,
                Statuses:data.Statuses
            }
        });
      return dialogRef;
    }

    openStatusDialog(data:IStatusDialogData){
        const dialogRef = this.dialog.open( ItemStatusDialogComponent, {
            width: '250px',
            data:data
          });
 
     return dialogRef;
    }
}
