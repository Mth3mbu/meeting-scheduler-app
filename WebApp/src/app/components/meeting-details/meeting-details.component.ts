import { IStatusDialogData } from './../../utility/interfaces/items-dialog-data';
import { IMeetingItem } from './../../utility/interfaces/item';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ITemService } from 'src/app/utility/services/item-service';
import { DialogService } from 'src/app/utility/services/DialogService';
import { UtilService } from 'src/app/utility/services/UtilService';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MeetingService } from 'src/app/utility/services/meeting-service';

@Component({
  selector: 'app-meeting-details',
  templateUrl: './meeting-details.component.html',
  styleUrls: ['./meeting-details.component.css'],
})
export class MeetingDetailsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private meetingItemService: ITemService,
    private meetingService: MeetingService,
    private dialog: DialogService,
    private snackBar: UtilService,
    private router: Router
  ) {}
  meetingItems: any;
  meetingName: string;
  isLoading: boolean = true;
  meetingItem: IMeetingItem = {
    Id: '',
    DueDate: null,
    PersonAssigned: '',
    Description: '',
    Status: '',
    StatusId: '',
    IsCarried: false,
    Statuses: [],
  };

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns = ['description', 'assigned', 'status', 'edit'];
  meetingId: string;
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.meetingId = params.id;
      this.meetingName = params.name;
    });
    this.getMeetingItems(this.meetingId);
    this.getStatuses();
  }

  openDialog(isAdd: boolean) {
    let dialogRef = this.dialog.openDialog(this.meetingItem);
    dialogRef.afterClosed().subscribe(
      (results) => {
        if (results == '') return;
        if (isAdd && results.Id != '') {
          this.createMeetingItem(results);
        }
      },
      (error) => {
        this.snackBar.openSnackbar(
          `An error occured while attempting to connect to the server`,
          'error-snackbar'
        );
      }
    );
  }

  openStatusDialog(element) {
    let data: IStatusDialogData = {
      id: element.StatusId,
      statuses: this.meetingItem.Statuses,
    };
    let dialogRef = this.dialog.openStatusDialog(data);
    this.meetingItem.Id = element.Id;
    dialogRef.afterClosed().subscribe(
      (results) => {
        if (results == undefined) return;
        this.meetingItem.StatusId = results.id;
        this.updateItemStatus();
      },
      (error) => {}
    );
  }

  onGotoMeetings(event) {
    this.meetingService.getMeetingByItemId(event.toElement.id).subscribe(
      (results) => {
        console.log(results);
        this.route.queryParams.subscribe((params) => {
          this.getMeetingItems(params.id);
          this.meetingName = params.name;
        });
        this.router.navigate(['details'], {
          queryParams: { id: results.Id, name: results.Name },
        });
      },
      (error) => {
        this.snackBar.openSnackbar(
          `An error occured while attempting to connect to the server please refresh the page`,
          'error-snackbar');
      }
    );
  }

  getMeetingItems(id: string) {
    this.meetingItemService.getMeetingItemsByMeetingId(id).subscribe(
      (results) => {
        this.meetingItems = new MatTableDataSource<IMeetingItem>(results);
        this.meetingItems.paginator = this.paginator;
        this.isLoading = false;
      },
      () => {
        this.snackBar.openSaveSnackbar(false);
      }
    );
  }

  getStatuses() {
    this.meetingItemService.getStatuses().subscribe(
      (results) => {
        this.meetingItem.Statuses = results;
      },
      () => {
        this.snackBar.openSaveSnackbar(false);
      }
    );
  }

  createMeetingItem(meetingItem: IMeetingItem) {
    this.meetingItemService
      .createMeetingItem(meetingItem, this.meetingId)
      .subscribe(
        () => {
          this.getMeetingItems(this.meetingId);
          this.snackBar.openSnackbar(
            `${meetingItem.Description} Saved`,
            'success-snackbar'
          );
        },
        () => {
          this.snackBar.openSnackbar(
            `An error occured while attempting to connect to the server please refresh the page`,
            'error-snackbar'
          );
        }
      );
  }

  updateItemStatus() {
    this.meetingItemService.updateMeetingItem(this.meetingItem).subscribe(
      () => {
        this.getMeetingItems(this.meetingId);
        this.snackBar.openSnackbar(
          `Status updated succesfully`,
          'success-snackbar'
        );
      },
      (error) => {
        this.snackBar.openSnackbar(
          `An error occured while attempting to fetch changes to the server please refresh the page`,
          'error-snackbar'
        );
      }
    );
  }
}
