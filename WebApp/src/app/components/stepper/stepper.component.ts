import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IType } from 'src/app/utility/interfaces/type';
import { MatPaginator } from '@angular/material/paginator';
import { MeetingService } from 'src/app/utility/services/meeting-service';
import { ITemService } from 'src/app/utility/services/item-service';
import { UtilService } from 'src/app/utility/services/UtilService';
import { Router } from '@angular/router';
import { IMeeting } from 'src/app/utility/interfaces/meeting';
import { MatTableDataSource } from '@angular/material/table';
import { IMeetingItem } from 'src/app/utility/interfaces/item';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css']
})
export class StepperComponent implements OnInit {

  nameFormGroup: FormGroup;
  dateFormGroup: FormGroup;
  typeFormGroup:FormGroup;
 
  meetingTypes: IType[] = [];
  itemsIds: string[] = [];
  meeting: IMeeting = {
    Id: '',
    Type: '',
    TypeId: '',
    DateCreated: '',
    Name: '',
  };
  items:any;
  displayedColumns = ['description', 'assigned', 'status', 'edit'];
  isOptional = false;
  disabled = true;
  isLoading =true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _formBuilder: FormBuilder,
    private meetingService: MeetingService,
    private meetingItemService: ITemService,
    private snackBar: UtilService,
    private router: Router) {}

  ngOnInit() {
   this.initialiseForm();
   this.meetingService.getMeetingTypes().subscribe((results) => {
    this.meetingTypes = results;
  });
  }

  onNext(){
    this.meeting.Name =this.nameFormGroup.value.name,
    this.meeting.DateCreated =this.dateFormGroup.value.dateCreated,
    this.meeting.TypeId =this.typeFormGroup.value.meetingTypeId;
  }

  onMeetingTypeChange(event) {
    let meetingTypeId = event.value;
    this.meetingItemService
      .getPreviousMeengItems(meetingTypeId)
      .subscribe((results) => {
        this.items =new MatTableDataSource<IMeetingItem>(results);
        this.items.paginator =this.paginator;
        this.isLoading =false;
      });
  }

  onItemChecked(event) {
    let itemid = event.source.id;
    if (event.checked) {
      this.itemsIds.push(itemid);
    } else {
      if (this.itemsIds.includes(itemid)) {
        this.itemsIds.splice(this.itemsIds.indexOf(itemid), 1);
      }
    }
  }

  onCreateMeeting() {
    let meetingId = Guid.create();
    this.meeting.Id = meetingId.toString();

    this.meetingService.CreateMeeting(this.meeting).subscribe(
      () => {
        if (this.itemsIds.length > 0) {
          this.forwardMeetingItems();
        }
        this.snackBar.openSnackbar(`${this.meeting.Name} created succesfully`,'success-snackbar')
        this.router.navigate(['/details'], { queryParams: { id: this.meeting.Id,  name: this.meeting.Name } });
      },
      () => {
        this.snackBar.openSnackbar(`An error occured while attempting to connect to the server`,'error-snackbar');
      }
    );
  }

  forwardMeetingItems() {
    this.meetingItemService
      .forwardMeetingItems(this.itemsIds, this.meeting.Id,true)
      .subscribe(
        () => {},
      );
  }

  initialiseForm(){
    this.nameFormGroup = this._formBuilder.group({
      name: ['', Validators.required]
    });
    this.dateFormGroup = this._formBuilder.group({
      dateCreated: ['', Validators.required]
    });
    this.typeFormGroup = this._formBuilder.group({
      meetingTypeId: ['', Validators.required]
    });
  }
}
