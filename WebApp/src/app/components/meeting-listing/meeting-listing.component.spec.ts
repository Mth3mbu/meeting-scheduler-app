import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingListingComponent } from './meeting-listing.component';

describe('MeetingListingComponent', () => {
  let component: MeetingListingComponent;
  let fixture: ComponentFixture<MeetingListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
