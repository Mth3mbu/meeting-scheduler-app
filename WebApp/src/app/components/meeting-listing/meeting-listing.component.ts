import { IMeeting } from './../../utility/interfaces/meeting';
import { Component, OnInit,ViewChild } from '@angular/core';
import { MeetingService } from 'src/app/utility/services/meeting-service';
import {MatPaginator} from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from 'src/app/utility/services/UtilService';

@Component({
  selector: 'app-meeting-listing',
  templateUrl: './meeting-listing.component.html',
  styleUrls: ['./meeting-listing.component.css'],
})
export class MeetingListingComponent implements OnInit {
  constructor(private meetingService: MeetingService, private snackBar: UtilService) {}
  displayedColumns= ['name','type', 'date','view'];
  meetings:any;
  isLoading:boolean=true;
  isRendered:boolean =false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
  ngOnInit(): void {
    this.meetingService.getAllMeetings().subscribe((results) => {
     this.meetings=new MatTableDataSource<IMeeting>(results);
     this.meetings.paginator=this.paginator;
     this.isLoading=false;
     this.isRendered=true;
    },error=>{
      this.snackBar.openSnackbar('An error occured while attempting to connect to the server','error-snackbar');
    });
  
  }
}
