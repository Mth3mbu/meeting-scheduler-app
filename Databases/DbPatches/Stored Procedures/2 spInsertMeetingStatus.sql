CREATE PROC spInsertMeetingStatus
	@status VARCHAR(100)
AS
BEGIN
	INSERT INTO [Status] VALUES(NEWID(),@status)
END