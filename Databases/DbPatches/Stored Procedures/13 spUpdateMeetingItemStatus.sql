CREATE PROC spUpdateMeetingItemStatus
	@id UNIQUEIDENTIFIER,
	@statusId UNIQUEIDENTIFIER
AS
BEGIN
	UPDATE MeetingItem SET [StatusId]=@statusId WHERE [Id]=@id
END