CREATE PROC spInsertMeeting
    @id UNIQUEIDENTIFIER,
    @typeId UNIQUEIDENTIFIER,
	@name VARCHAR(100),
	@date SMALLDATETIME
AS
BEGIN
	INSERT INTO Meeting VALUES(@id,@typeId,@name,CAST(@date  AS smalldatetime))
END