CREATE PROC spLinkMeetingItem
	@meetingId UNIQUEIDENTIFIER,
	@itemId UNIQUEIDENTIFIER,
	@isCarried BIT
AS
BEGIN
	INSERT INTO LinkMeetingItem VALUES(NEWID(),@meetingId,@itemId,@isCarried,CAST(SYSDATETIME() AS smalldatetime))
END

