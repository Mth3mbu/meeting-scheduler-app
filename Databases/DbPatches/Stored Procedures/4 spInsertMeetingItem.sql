CREATE PROC spInsertMeetingItem
	@id UNIQUEIDENTIFIER,
	@statusId UNIQUEIDENTIFIER,
	@personAssigned VARCHAR(100),
	@description VARCHAR(250),
	@dueDate DATETIME
AS
BEGIN
	INSERT INTO MeetingItem VALUES (@id,@statusId,@personAssigned,@description,CAST(@dueDate AS SMALLDATETIME))
END