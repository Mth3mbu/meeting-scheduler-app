CREATE PROCEDURE spGetAllMeetings
AS
BEGIN
 SELECT m.[ID],m.[Name],m.[DateCreated],t.[Type] FROM Meeting m 
 LEFT JOIN MeetingType t ON m.[TypeId] =t.[Id]
 WHERE m.[TypeId] =t.[Id]
END