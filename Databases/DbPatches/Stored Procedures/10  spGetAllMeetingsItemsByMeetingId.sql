CREATE PROC spGetAllMeetingsItemsByMeetingId
	@meetingId UNIQUEIDENTIFIER
AS
BEGIN
	 SELECT i.[StatusId],i.[Id],i.[PersonAssigned],i.[Description],l.[IsCarried],i.[DueDate],s.[Status] FROM MeetingItem i 
	 LEFT JOIN LinkMeetingItem l ON l.[ItemId]=i.[Id]
	 LEFT JOIN [Status] s ON s.[Id]=i.[StatusId] 
	 LEFT JOIN Meeting m ON m.Id=l.MeetingId
	 WHERE l.MeetingId =@meetingId
END