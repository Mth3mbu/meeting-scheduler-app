ALTER PROC [dbo].[spLinkMeetingItems]
 @items ItemsUDT READONLY
AS
BEGIN
	INSERT INTO [dbo].[LinkMeetingItem]
           ([Id]
           ,[MeetingId]
           ,[ItemId]
           ,[Iscarried]
           ,[DateCreated])
           SELECT CAST([Id] AS uniqueidentifier)T,CAST([MeetingId] AS uniqueidentifier),CAST([ItemId] AS uniqueidentifier),[Iscarried],[DateCreated] FROM  @items
END
