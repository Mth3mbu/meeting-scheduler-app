CREATE PROC spInsertMeetingType
	@type VARCHAR(100)
AS
BEGIN
	INSERT INTO MeetingType VALUES (NEWID(),@type)
END