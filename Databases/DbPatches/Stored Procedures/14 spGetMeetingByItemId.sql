CREATE PROC spGEtMeetingByItemId
@itemId UNIQUEIDENTIFIER
AS
BEGIN
 SELECT TOP 1 m.[Id],m.[Name] FROM Meeting m
 JOIN LinkMeetingItem l ON l.[MeetingId] =m.[Id]
 WHERE l.[Iscarried]=0
END
