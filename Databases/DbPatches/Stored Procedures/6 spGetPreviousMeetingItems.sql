CREATE PROC spGetPreviousMeetingItemsByMeetingType
	@typeId UNIQUEIDENTIFIER
AS
BEGIN
	 DECLARE @meetingId UNIQUEIDENTIFIER=(SELECT TOP 1 [ID] FROM Meeting  WHERE [TypeId]=@typeId ORDER BY [DateCreated] DESC)
	 SELECT i.[StatusId],i.[Id],i.[PersonAssigned],i.[Description],l.[IsCarried],i.[DueDate],s.[Status] FROM MeetingItem i 
	 LEFT JOIN LinkMeetingItem l ON l.[ItemId]=i.[Id]
	 LEFT JOIN [Status] s on s.[Id]=i.[StatusId]
	 WHERE l.MeetingId =@meetingId
END