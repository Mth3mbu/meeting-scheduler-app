
CREATE TABLE MeetingType(
	Id UNIQUEIDENTIFIER PRIMARY KEY NOT NULL,
	[Type] VARCHAR(100) NOT NULL
)