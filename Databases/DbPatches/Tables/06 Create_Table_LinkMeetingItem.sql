CREATE TABLE LinkMeetingItem(
 [Id] UNIQUEIDENTIFIER PRIMARY KEY NOT NULL,
 [MeetingId] UNIQUEIDENTIFIER FOREIGN KEY REFERENCES Meeting([Id]) NOT NULL,
 [ItemId] UNIQUEIDENTIFIER FOREIGN KEY REFERENCES MeetingItem([Id]) NOT NULL,
 [Iscarried] BIT NOT NULL,
 [DateCreated] SMALLDATETIME NOT NULL
)